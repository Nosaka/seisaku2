# -*- coding: utf-8 -*-

class User

  # Userクラスのインスタンスを作る
  def initialize(dbh)
    @dbh = dbh
    @logined = false
    @name = ""
  end

  # 属性にアクセサを提供する
  attr_accessor :logined, :name

  # ユーザーの登録機能
  def register
    # ユーザ登録を行う
    print "あなたの名前を入力してください: "
    name = gets.chomp
    # ユーザ情報を取得
    sth = @dbh.execute("select name from users where name = '#{name}'")
    if sth.first then
      # 登録済みエラーを表示
      print "そのユーザ名は既に使用されています。"
    else
      # ユーザ情報の登録処理を行う
      @dbh.execute("insert into users (name) values ('#{name}')")
      @logined = true
      @name = name
      print "#{name}でユーザ登録を行いました。"
    end
    # 実行結果を解放する
    sth.finish
  end

  # ログイン機能
  def login
    # ログインを行う
    print "あなたのユーザ名を入力してください: "
    name = gets.chomp
    # ユーザ情報取
    sth = @dbh.execute("select name from users where name = '#{name}'")
    # ユーザ名が合致するか調べる
    if sth.first then
      @logined = true
      @name = name
      print "#{name}でログインしました。"
    else
      # 未登録エラーを表示
      print "一致するユーザ名はありません。"
    end
    # 実行結果を解放する
    sth.finish
  end
end
