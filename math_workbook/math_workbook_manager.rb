require 'rdbi'       # RDBIを使う
require 'rdbi-driver-sqlite3' # RDBIでSQLite3と接続するドライバ

# MathWorkManagerクラスを定義する
class MathWorkManager
  def initialize( sqlite_name )
    # SQLiteデータベースファイルに接続
    @db_name = sqlite_name
    @dbh = RDBI.connect( :SQLite3, :database => @db_name )
  end
end
