# 共通で利用するユーティリティー
# example:
# include CommonUtilities
#
# version 1.0.0
# since 1.0.0 -
# author 
# copyright 
module CommonUtilities

  # 大きい値を取得します
  # 同じ値の場合はそれを返します
  #
  # param [Integer] val1 : 比較する値１
  # param [Integer] val2 : 比較する値２
  # return [Integer] : val1とval2の大きい方の値
  # example:
  # larger(5, 7) #=> 7
  #
  def larger(val1, val2)
    return val1 > val2 ? val1 : val2
  end

  # 小さい値を返します
  # 同じ値の場合はそれを返します
  #
  # param [Integer] val1 : 比較する値１
  # param [Integer] val2 : 比較する値２
  # return [Integer] : val1とval2の小さい方の値
  # example:
  # larger(5, 7) #=> 5
  #
  def smaller(val1, val2)
    return val1 < val2 ? val1 : val2
  end

  # 指定の確率でtrueを返します。
  #
  # param [Integer] weight : 確率を指定します。
  # return [Boolean] : weightで指定した確率に一致した場合trueを返します。
  # example:
  # choose(80) #=> 80%の確率でtrue
  #
  def choose(weight = 50)
    rand <= weight/100.0
  end
end