# -*- coding: utf-8 -*-
require 'rdbi'   # RDBIを使う
require 'rdbi-driver-sqlite3'   # RDBIでSQLite3と接続するドライバ
require './user'

class MathWorkbook

  def initialize( sqlite_name )
    # SQLiteデータベースファイルに接続
    @db_name = sqlite_name
    @dbh = RDBI.connect( :SQLite3, :database => @db_name )
    @last_time_offset = nil;
    @login_user_name = "";
    @logged_in = false
  end
  
   # ランキング参照機能
  def ranking_reference
    puts "\n\n"
    puts "------------------------------------"
    puts "ランキング上位5名はこの人たちです"
    puts "\n"
    puts "---------------------"
    #usersのデータを取り出し、recordの数値が高い順に5件表示する
    sth = @dbh.execute("select * from users order by record desc limit 5")
    # テーブルの項目名を配列で取得する
    columns = sth.schema.columns.to_a
    # select文の実行結果を1件ずつrowに取り出し、繰り返し処理する
    sth.each do |row|
      # rowは1件分のデータを保持している
      row.each_with_index do |val, index|
        puts "#{columns[index].name}: #{val.to_s}"
      end
    end   
    puts "---------------------"
  end

  # 計算ドリル機能
  def start
    puts "

------------------------------------------------------------------"
    puts "  問題を５問出します。(※未ログインの場合、成績は登録されません)"
    puts "------------------------------------------------------------------"
    offsets = [0, 5, 10, 15]
    offsets.delete(@last_time_offset)
    offset = offsets.sample
    # offset = offsets.reject{|item| item == @last_time_offset }.sample
    @last_time_offset = offset
    limit = 5
    # 問題を取得
    sth = @dbh.execute("select * from exercises limit #{limit} offset #{offset}")
    columns = sth.schema.columns.to_a
    correct_count = 0
    # select文の実行結果を１件ずつrowに取り出し、繰り返し処理をする
    sth.each do |row|
      answer = ""
      # rowは１件分のデータを保持
      row.each_with_index do |val, index|
        case columns[index].name
        when :calculation
          puts "\n\n"
          puts "   #{val.to_s}"
        when :answer
          answer =  val.to_s
        end
      end
      
      # 解答を入力
      print "   解答を入力してください: "
      input_answer = gets.chomp
      puts "--------------------------------"
 
      # 正否判定、正否表示
      if answer == input_answer then
        puts "   正解、答えは#{answer}です。"
        correct_count += 1
      else
        puts "   不正解、答えは#{answer}です。"
      end      
    end
    # 成績表示・成績登録
    puts "\n\n\n\n"
    puts "  お疲れ様でした。
  正解数は#{correct_count}です。"
    puts "\n"
    if @logged_in then
      @dbh.execute("update users set record = #{ correct_count} where name = '#{@login_user_name}'")
    end
    # 実行結果を解放する
    sth.finish
  end  

  # 処理の選択と選択後の処理を繰り返す
  def run
    user = User.new(@dbh)
    puts "\e[H\e[2J"
    puts '

 /$$      /$$             /$$     /$$             /$$      /$$                     /$$       /$$                           /$$      
| $$$    /$$$            | $$    | $$            | $$  /$ | $$                    | $$      | $$                          | $$      
| $$$$  /$$$$  /$$$$$$  /$$$$$$  | $$$$$$$       | $$ /$$$| $$  /$$$$$$   /$$$$$$ | $$   /$$| $$$$$$$   /$$$$$$   /$$$$$$ | $$   /$$
| $$ $$/$$ $$ |____  $$|_  $$_/  | $$__  $$      | $$/$$ $$ $$ /$$__  $$ /$$__  $$| $$  /$$/| $$__  $$ /$$__  $$ /$$__  $$| $$  /$$/
| $$  $$$| $$  /$$$$$$$  | $$    | $$  \ $$      | $$$$_  $$$$| $$  \ $$| $$  \__/| $$$$$$/ | $$  \ $$| $$  \ $$| $$  \ $$| $$$$$$/ 
| $$\  $ | $$ /$$__  $$  | $$ /$$| $$  | $$      | $$$/ \  $$$| $$  | $$| $$      | $$_  $$ | $$  | $$| $$  | $$| $$  | $$| $$_  $$ 
| $$ \/  | $$|  $$$$$$$  |  $$$$/| $$  | $$      | $$/   \  $$|  $$$$$$/| $$      | $$ \  $$| $$$$$$$/|  $$$$$$/|  $$$$$$/| $$ \  $$
|__/     |__/ \_______/   \___/  |__/  |__/      |__/     \__/ \______/ |__/      |__/  \__/|_______/  \______/  \______/ |__/  \__/
                                                                                                                                    
                                                                                                                                                                                                                                                                     '
    loop do
      # 機能選択画面を表示する
      print "
   -----------------------------------
      1. ユーザ登録
      2. ログイン
      3. ランキング参照
      4. 計算ドリル開始
      9. アプリ終了
   ----------------------------------- 
      番号を選んでください(1,2,3,4,9): "

      # 文字の入力を待つ
      num = gets.chomp
      case
      when '1' == num
        user.register
        @login_user_name = user.name
        @logged_in = user.logined
      when '2' == num
        user.login
        @login_user_name = user.name
        @logged_in = user.logined
      when '3' == num
        ranking_reference
      when '4' == num
        start
      when '9' == num
        @dbh.disconnect
        puts "\n\n"
        puts "   アプリを終了しました"
        puts "\n\n"
        break
      end
    end
  end
end

mathworkbook = MathWorkbook.new("math_workbook.db")
mathworkbook.run
