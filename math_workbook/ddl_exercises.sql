drop table if exists exercises;
create table exercises (
    id               integer primary key, 
    calculation      string not null,
    answer           string not null
);
