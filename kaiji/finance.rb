#require 'rdbi'
#require 'rdbi-driver-sqlite3'

module Finance
  def initialize
#   @dbh = RDBI.connect(:SQLite3, :database => "kaiji.db")
  end
=begin  デバッグ用
  def debug
    @money = @dbh.execute("select money from gomimera where id = 1").fetch[0]
    @debt = @dbh.execute("select debt from gomimera where id = 1").fetch[0]
    puts @money
    puts @debt.class
    if @money[0] < 1000000000
    	puts "とれてる"
    else 
      puts "とれてない"
    end

    @dbh.disconnect
  end   
=end
  def acceptance
    puts "\e[H\e[2J"
  	puts "黒服「ククク…ここは借金の貸し付けと返済を受け付けている窓口だ」▼"
  	while true
  	  puts "黒服「用件を聞こう」"
  	  puts "1:借り入れ"
  	  puts "2：返済"
  	  puts "9:戻る"
  	  cmd = gets.chomp.to_i        
      case
        when cmd == 1
          borrowing
        when cmd == 2
          payback
        when cmd == 9
         # @dbh.disconnect
          break;
      end   
    end
  end

  def borrowing
  	puts "黒服「いくら入用だ？貸し付けは最低100万、100万単位で受け付ける」"
  	amount = gets.chomp.to_i
  	if amount < 1000000 or amount % 1000000 != 0
  	  puts "黒服「その金額では受け付けられんな」"
    else
      puts "黒服「#{amount}ペリカだな、利息は5割、返済は#{amount + amount/2}ペリカだ」"
      while true
        puts "黒服「本当に借りるか？」"
        puts "1:借りる\n2：借りない"
        yon = gets.chomp.to_i
        case 
          when yon == 1
            puts "黒服「いいだろう、待っていろ。用意してやる。」"
            @dbh.execute("update gomimera set money = money + #{amount}, debt = debt + #{amount + amount/2} where id = #{@id}")
            sleep(1)
            @debt = @dbh.execute("select debt from gomimera where id = #{@id}").fetch
            @money = @dbh.execute("select money from gomimera where id = #{@id}").fetch           
            puts "黒服「ほれ、これでお前の借金額は#{@debt[0]}ペリカだ。まぁ、せいぜい頑張って返してくれ」▼"
            gets
            puts "所持金が#{@money[0]}ペリカになった。"
            break;
          when yon == 2
            puts "「やめるのか。まぁ、それもいいがな・・・ククク・・・」▼"
            gets
            break;
        end
      end
    end 
  end

  def payback
    @debt = @dbh.execute("select debt from gomimera where id = #{@id}").fetch[0]
    puts "黒服「お前の借金は現在#{@debt}だ。いくら返済するのだ？」"
  	amount = gets.chomp.to_i
  	if amount > @debt[0]
  		puts "黒服「何を言っているんだ？」"
    else
      puts "黒服「#{amount}ペリカ返済するんだな？」"
      while true
        puts "黒服「本当に良いか？」"
        puts "1:返済する\n2：返済しない"
        yon = gets.chomp.to_i
        case 
          when yon == 1
            puts "黒服「いいだろう、確かに預かった」"
            @dbh.execute("update gomimera set money = money - #{amount}, debt = debt - #{amount} where id = #{@id}")
            sleep(1)
            @debt = @dbh.execute("select debt from gomimera where id = #{@id}").fetch[0]
            @money = @dbh.execute("select money from gomimera where id = #{@id}").fetch[0]          
            puts "黒服「ほれ、これでお前の借金額は#{@debt[0]}ペリカだ。」▼"
            gets
            puts "所持金が#{@money[0]}ペリカになった。借金が#{@debt[0]}ペリカになった。"
            break;
          when yon == 2
            puts "「やめるのか。まぁ、それもいいがな・・・ククク・・・」▼"
            gets
            break;
        end
      end   
    end
  end
end
 

