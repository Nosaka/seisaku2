# -*- coding: utf-8 -*-

module DiceBattle
  def initialize
    @num = [1,2,3,4,5,6]
    @outcomes = []
    @privilege = 0
    @bet_price = 0
    @player_score = 0
    @com_score = 0
  end

  def dice_battle
    while true
      @num = [1,2,3,4,5,6]
      # 選択肢を表示する
      puts "班長「開帳だっ・・・！チンチロ・・・」"
      print "
1: 勝負する
2: 説明を受ける
3: 勝負しない\n"
      input_num = gets.chomp.to_i
      case input_num
      when 1
        # プレイヤーの所持金を取得する。
        @money = @dbh.execute("select money from gomimera where id = #{@id}").fetch[0]
        print "
班長「さぁ、#{@name}くんよ、いくら張ってくれるんだい・・・最低5万、天井は20万・・・」"
        puts "\n「どうする・・・」
5～20の範囲で入力してください："
        input_bet = gets.chomp.to_i
        sth = @dbh.execute("select money from gomimera where id = #{@id}")
        if input_bet.to_i < 5 || input_bet > 20 then
          puts "５～２０の範囲で入力してください"
        elsif input_bet * 100000 > sth.first[0].to_i then
          puts "BET金額の10倍の所持金が必要です"
        else
          @bet_price = input_bet * 10000
          # 3.COMがダイスを振る処理
          puts "班長「では・・・参ろうか・・・」"
          count = 1
          3.times {
            puts "#{count}投目・・・"
            sleep(1)
            shake_the_dice
            if @score
              @com_score = @score
              break
            else
              @com_score = 0
              count += 1
            end
          }
          # 4.判定処理へ
          puts "▼"
          gets
          puts "班長「さぁ・・・#{@name}くん・・・」"
          count = 1
          3.times {
            puts "(#{count}投目・・・)▼"
            gets
            shake_the_dice
            if @score
              @player_score = @score
              break
            else
              @player_score = 0
              count += 1
            end
          }
          # 配当処理
          puts "\n▼"
          gets

          if @com_score == @player_score then
            puts ""
          else
            sth = @dbh.execute("update gomimera set money = money + #{shared} where id = #{@id}")
            if @com_score > @player_score then
             puts "班長「フフフ・・・悪いな・・・」"
             puts "#{shared}ペリカ支払った。"
           else
             puts "「よし・・・勝ったぞっ・・・！」"
             puts "#{shared}ぺリカ手に入れた。"
           end
         end
       end

      when 2
        puts "班長「わかったわかった・・・順を追って説明しよう」▼"
        gets
        dicebattle_rule
      # 勝負をしないを選択
      when 3
        puts "班長「へたっぴさ・・・欲望の解放のさせ方が下手・・・！」"
        break;
      end
    end
  end
#１～６の目をランダムに3回、配列に格納する
  def shake_the_dice
    @outcomes = []
    3.times {
      @outcomes.push(@num.sample)
    }
    @outcomes.sort!
    privilege
  end

#出目の判定
  def privilege
    p @outcomes
    @score = nil

    if @outcomes[0] == @outcomes[1] && @outcomes[1] == @outcomes[2]
      @privilege = @outcomes[0]
      puts "#{@privilege}ゾロっ・・・！"
      case @privilege
        when 1
          @score = 9
        else
          @score = 8
      end

    elsif @outcomes[0] == @outcomes[1] && @outcomes[1] != @outcomes[2]
      @privilege = @outcomes[2]
      puts "#{@privilege}の目"
      @score = @privilege

    elsif @outcomes[0] != @outcomes[1] && @outcomes[1] == @outcomes[2]
      @privilege = @outcomes[0]
      puts "#{@privilege}の目"
      @score = @privilege

    elsif @outcomes[0] == 1 && @outcomes[1] == 2 && @outcomes[2] == 3
      puts "123(ヒフミ)っ・・・！"
      @score = -1

    elsif @outcomes[0] == 4 && @outcomes[1] == 5 && @outcomes[2] == 6
      puts "456(シゴロ)っ・・・！"
      @score = 7
    end
    puts @score
  end

  # 取り分の判定
  def shared
    increase_or_decrease = 0
    # 配当倍率を決定する（勝ちの倍率を決定）
    case @player_score > @com_score ? @player_score : @com_score
    when 9
      increase_or_decrease = 5
    when 8
      increase_or_decrease = 3
    when 7
      increase_or_decrease = 2
    when 0..6
      increase_or_decrease = 1
    end
    # 配当倍率を決定する（負けの倍率を決定）
    if @player_score < @com_score ? @player_score : @com_score == -1 then
      increase_or_decrease *= 2
    end
    # 配当金額を決定する
    dividend = @bet_price * increase_or_decrease
    if @player_score < @com_score then
      dividend = -dividend
    end
    return dividend

    if @player_score == @com_score
      puts "班長「フフフ・・・引き分け・・・ノーカンっ・・・！」"
    end
  end

  def dicebattle_rule
    puts "\e[H\e[2J"
    puts "班長「簡単さ・・・チンチロってのはサイコロを3つどんぶりに投げ込んで・・・
その目で勝ち負けが決まる博奕だ。▼"
    gets
    puts "これが・・・その目の強い順だ・・・」\n"
    puts "--------------------
ピンゾロ(1・1・1・)：5倍
--------------------
ゾロ目（1以外）：3倍
--------------------
シゴロ(4・5・6)：2倍
--------------------
6の目～1の目：1倍
(1<2<3<4<5<6)
--------------------
目なし：1倍
--------------------
ヒフミ(1・2・3)：-2倍
--------------------
▼"
  gets
  puts "班長「要するに・・・[2,4,5]なんかは目なしってことに・・・
[3,3,6]こんな風に2つ揃ったときは・・・揃わなかったほう、この場合は6。
これがチンチロの出目さ」▼"
  gets
  puts "班長「目が出なくても3回までは振れるが・・・3回振っても目無しってことはしょっちゅうさ・・・
目無しより悪い目はヒフミ[1,2,3]だけ」▼"
    gets
    puts "班長「まず親であるわしが振り、そのあと子の#{@name}くんが振る。
じっくり楽しみたいという・・・一種の貧乏性でよ・・・親の総取りは無し・・・
いくら親が強い目だからって子が振ることもできないってのはナシ。」▼"
    gets
    puts "班長「最後に・・・賭け金は最低5万ペリカ、どんなに張っても20万ペリカまで・・・。
まぁ、こんなところだ・・・」▼"
    gets
  end
end

