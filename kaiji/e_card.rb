module ECard

  def initialize
    @emperor = [] 
    @slave = []
  end

  def ecard
      puts "\e[H\e[2J"
    user = @dbh.execute("select name, money from gomimera where id = #{@id}").fetch[0]
    @name = user[0]
    @money = user[1]
    while true
      puts "利根川「ようこそ、Eカードへ」"
      puts "1:勝負する
2：説明を受ける
9：勝負しない。"
      cmd = gets.chomp.to_i
      case
        when cmd == 1
          if @money < 2000000
          	puts "利根川「うつけものがっ・・・足らんだろう・・・金っ！」▼"
          	gets
           	break;
          end
          3.times do
          	@round = 1
            e_card_start
            if @money < 2000000
              puts "利根川「ククク・・・金が尽きたか。ここまでだな・・・」▼"
              gets
              puts "\e[H\e[2J"
              break;
            end
            @round += 1
          end
          puts "\n利根川「また挑戦してくれたまえ」▼"
          gets
        when cmd == 2
          puts "\n利根川「ククク・・・ルールを説明してやるからよく聞けよ・・・」▼"
          gets
          e_card_rule
        when cmd == 9
          puts "利根川「ククク・・・それもよかろう」"
        break;
      end
    end
  end

  def e_card_start
    emperor = ["市民", "市民", "市民", "市民","皇帝"]
    slave = ["市民", "市民", "市民", "市民","奴隷"]
    puts "利根川「では・・・#{@round}回戦といこうか・・・いくら賭けるかね？"
    while true
      puts "最低100万、最大1000万の100万ペリカ刻みだ。」"
      puts "所持金：#{@money}ペリカ"
      @bet = gets.chomp.to_i
      if @bet * 2 <= @money && 1000000 <= @bet && @bet <= 10000000 && @bet % 1000000 == 0
        break;
      else
        puts "利根川「その金額では受けられんな。」"
      end
    end

    sleep(1)
    puts "\n「勝ってやる・・・絶対に・・・！」▼\n"
    gets
    puts "利根川「ククク・・・では皇帝と奴隷の抽選を行う。」▼"
    gets
    turn = [0,1]
    your_turn = turn.sample
    if your_turn == 0
      @you = "皇帝"
      @enemy = "奴隷"
      @your_cards = emperor
      @enemy_cards = slave
      puts "\n「【#{@you}】側か・・・よし！」"
      puts "\n#{@name}の手札:"
      puts "#{@your_cards}▼"
      gets
      puts "\n利根川「ふむ・・・」"
      puts "\n利根川の手札:"
      puts "#{@enemy_cards}▼"
      gets
      play
    else
      @you = "奴隷"
      @your_cards = slave
      @enemy_cards = emperor
      puts "\n「くっ・・・【#{@you}】側か・・・」"
      puts "\n#{@name}の手札:"
      puts "#{@your_cards}▼"
      gets
      puts "\n利根川「ククク・・・勝たせてもらうぞ・・・#{@name}っ・・・！」"
      puts "\n利根川の手札:"
      puts "#{@enemy_cards}▼"
      gets
      play
    end
  end
  def play
    @wl_flg = 0
    count = 1
    while @wl_flg == 0
      if @your_cards.length == 1
        case 
          when @your_cards[0] == "皇帝"
            @wl_flg = 1
            break;
          else
            @wl_flg == 2
            break;
        end
      end
    
    while true
      sleep(1)
      puts "\n何を出す・・・？"
      puts "1: 市民 9: #{@you}"
      num = gets.chomp.to_i
      case
        when num == 1
          puts "「ここは市民で様子見だ・・・！」"
          your_choice = @your_cards[0]
          break;
        when num == 9
          puts "「ここで殺(と)るっ・・・！#{@you}で勝負っ・・・！」"
          your_choice = @your_cards[-1]
          break;
      end
    end
      sleep(1)
      puts "\n互いの#{count}枚目・・・！"
      sleep(1)
      enemy_choice = @enemy_cards.sample
      puts "\n#{@name}の選出：#{your_choice}"
      sleep(1)
      puts "\n利根川「・・・」"
      sleep(2)
      puts "\n利根川の選出：#{enemy_choice}"
      sleep(1)
      case
        when your_choice == "市民" && enemy_choice == "奴隷"
          puts "\n勝ち"
          @wl_flg += 1
        when your_choice == "市民" && enemy_choice == "皇帝"
          puts "\n負け"
          @wl_flg += 2
        when your_choice == "皇帝" && enemy_choice == "市民"
          puts "\n勝ち"
          @wl_flg += 1
        when your_choice == "皇帝" && enemy_choice == "奴隷"
          puts "\n負け"
          @wl_flg += 2
        when your_choice == "奴隷" && enemy_choice == "皇帝"
          puts "\n勝ち"
          @wl_flg += 1
        when your_choice == "奴隷" && enemy_choice == "市民"
          puts "\n負け"
          @wl_flg += 2
        when your_choice == "市民" && enemy_choice == "市民"
          puts "\n引き分け"
          count += 1
          @your_cards.delete_at(0)
          @enemy_cards.delete_at(0)
      end
    end
    case
    when @wl_flg == 1
      puts "\n「やった・・・勝ったぞっ・・・！」\n"
      sleep(1)
      puts "\n利根川「馬鹿な・・・・・・！」\n"
    when @wl_flg == 2
      puts "\n「あ・・・あ・・・」\n"
      sleep(1)
      puts "\n利根川「残念だったな#{@name}・・・」\n"
      sleep(1)
      puts "\n「くそっ・・・くそっ・・・！」\n"
    end
    sleep(2)
    puts "\n--圧倒的決着っ！▼"
    gets
    puts "\e[H\e[2J"
    dividend
  end

  def dividend
    case
      when @wl_flg == 1
        if @you == "皇帝"
          @dbh.execute("update gomimera set money = money + #{@bet} where id = #{@id}")
          puts "#{@bet}ペリカ獲得"
        else
          @dbh.execute("update gomimera set money = money + #{@bet * 3} where id = #{@id}")
          puts "#{@bet *3}ペリカ獲得"
        end
      when @wl_flg == 2
        if @you == "皇帝"
          @dbh.execute("update gomimera set money = money - #{@bet * 2} where id = #{@id}")
          puts "#{@bet * 2}ペリカ失った"
        else
          @dbh.execute("update gomimera set money = money - #{@bet} where id = #{@id}")
          puts "#{@bet}ペリカ失った"
        end
    end
    money = @dbh.execute("select money from gomimera where id = #{@id}").fetch[0]
    @money = money[0]
    puts "所持金が#{@money}ペリカになった▼"
    gets
  end

  def e_card_rule
    puts "\e[H\e[2J"
    puts "利根川「『Eカード』は3種類のカード10枚を使って行うギャンブルだ。
使うカードは【市民】・【皇帝】・【奴隷】▼"
    gets
    puts "とはいえこのEカード・・・10枚中8枚までが【市民】。つまり・・・
【皇帝】と【奴隷】は特殊なカードで、1枚ずつしかない・・・▼"
    gets
    puts "この10枚を互いに5枚ずつ分けて闘う、すなわち・・・
【皇帝】側と【奴隷】側にだ・・・▼"
    gets
    puts "勝負は互いに互いの手札が見えないよう1枚を選択する。
両名の手札が選択されたら開く・・・これだけだ。▼"
    gets
    puts "カードがぶつかったときの勝敗は【皇帝】は【市民】に強く、【市民】は【奴隷】に強い。▼"
    gets
    puts "【奴隷】は持たざる者・・・虐げられし者・・・しかし・・・その何も持たぬ・・・
劣悪な環境であるが故に・・・【皇帝】を討つのだ・・・！▼"
    gets
    puts "【市民】と【市民】がぶつかった場合、勝負無し。引き分け・・・▼"
    gets
    puts "【皇帝】側は【市民】に紛れて・・・いかに【皇帝】を通すか。
逆に【奴隷】側はこれを許してはならぬ・・・▼"
    gets
    puts "単純さ・・・実に簡単な心理戦・・・
しかし・・・これが中々奥深い・・・！」▼"
    gets
    puts "\n利根川「賭け金は最低100万ペリカ。上限1000万ペリカまで受け付ける・・・
配当は【皇帝】側の勝利であれば1倍、不利な【奴隷】側での勝利であれば3倍を用意しよう。
しかし、#{@name}が負けても3倍払いは痛かろう。プレイヤー側は【皇帝】での負け分は2倍払だ。
これはカジノ側からのプレイヤーに対する配慮だ。▼"
    gets
    puts "ただし、どちらの陣営に当てがわれるかは運否天賦・・・！
BETが終わりゲームが開始した後にCPUが自動的に抽選する・・・！
ククク・・・気を付けてBETしたまえ。▼"
    gets
    puts "勝負は全3ラウンド消化するか、君の所持金が200万ペリカ未満になれば終了だ・・・。
説明は以上だ。健闘を祈るよ」▼"
    gets
    puts "\e[H\e[2J"
  end

end
