$:.unshift File.dirname(__FILE__)
require 'common_utilities'

module OnePorker
  include CommonUtilities

  def initialize
    @number = [1,2,3,4,5,6,7,8,9,10,11,12,13]
    @deck = []
    @player_hand = []
    @enemy_hand = []
    @enemy_hol = []
    @com_select_card = 0
    @player_select_card = 0
    @round = 0
  end
  
  def oneporker
    user = @dbh.execute("select money from gomimera where id = #{@id}").fetch[0]
    @money = user[0]
    while true
      puts "\e[H\e[2J"
      puts "和也「カカカ・・・#{@name}・・・俺とお前で勝負だ・・・このワン・ポーカーでっ・・・！」"
      puts "1:勝負する
2：説明を受ける
9：勝負しない。"
      cmd = gets.chomp.to_i
      case
        when cmd == 1
          if @money < 20000000
            puts "和也「あ・・・？足りねえじゃねぇか・・・金っ・・・！」\n"
            break;
          end
          shuffle
          @round = 1
          5.times do
            puts "\e[H\e[2J"
            puts "-#{@round}戦目▼"
            gets
            start
            if @money < 20000000
              puts "和也「カカカ・・・ここまでだ、#{@name}。ゲームオーバーっ・・・！沈没っ・・・！」▼"
              gets
              puts "\e[H\e[2J"
              break;
            end
            @round += 1
          end
          puts "-ゲームセット！"
          puts "\n和也「またやろうや・・・#{@name}」▼"
          gets
        when cmd == 2
          puts "\n和也「ククク・・・分かってるよ・・・そう焦るな・・・」▼"
          gets
          rule
        when cmd == 9
          puts "和也「あ・・・？なんだ・・・やめるのか・・・」"
        break;
      end
    end
  end

#配列にトランプ52枚×3セット分の数字を入れ、その山を半分にする
  def shuffle
    puts "\e[H\e[2J"
    puts "和也「山札は新たな3セットからジョーカーを抜いて、残りをシャッフル。
混ぜていいぞ、#{@name}も！遠慮するな」▼"
    gets
    puts "「・・・」▼" 
    gets
    12.times {
      @number.each {|number| 
      	@deck.push(number)
      }
    }
	@deck.shuffle!
  puts "和也「そうしたら半分にカット・・・下半分のカードを使い、残りは捨てる▼"
  gets
	@deck.slice!(0...@deck.size/2)
  puts "今決まったぜ！この勝負の運命が！始めるか・・・！一張り2000万のギャンブル・・・！」▼"
  gets
#	start
  end

  def start
    # ドローする
    drow_num = @round == 1 ? 2 : 1
    drow_num.times do
      @player_hand.push(@deck.pop)
      @enemy_hand.push(@deck.pop)
    end
    @enemy_hol = []
    2.times do |i|
      if @enemy_hand[i] > 7
  	@enemy_hol.push("up")
      elsif @enemy_hand[i] == 1 
        @enemy_hol.push("up")
      else 
        @enemy_hol.push("down")
      end
    end
    puts "和也手札：#{@enemy_hol}▼"
    gets
    # 要確認
    # 手札1枚ずつの数値によってUPかDOWNかを表示させる。
    # プレイヤーのターン
    puts "#{@name}手札：#{@player_hand}"
    puts "-プリーズ、セレクト、#{@name}"
    loop do
      print "
1: #{@player_hand[0]}
2: #{@player_hand[1]}
: "
      select_num = gets.chomp
      case select_num
      when "1"
        @player_select_card = @player_hand.delete_at(0)
        break
      when "2"
        @player_select_card = @player_hand.delete_at(1)
        break
      else
        puts "１か２を選べ"
      end
    end
    # COMのターン
    com_choose
    # 勝負する
    puts "\e[H\e[2J"
    puts "-カード、オープン\n"
    sleep 2
    puts "#{@name}の手札は#{@player_select_card}\n"
    sleep 2
    puts "和也の手札は#{@com_select_card}\n"
    puts "▼"
    gets
    # 引き分け
    if @player_select_card == @com_select_card
      puts "引き分け"
    else
      # プレイヤーの勝ち
           if (@player_select_card == 1 && @com_select_card !=2) ||
       (@player_select_card == 2 && @com_select_card ==1) ||
       (@com_select_card !=1 && @player_select_card > @com_select_card)
        puts "-ウィナー、#{@name} ウィナー、#{@name}\n"
        puts "和也「ぐっ！ぐぐぐ・・・！」"
        puts "「よしっ・・・！」▼\n"
        gets
        shared = 20000000
        puts "#{shared}ペリカ手に入れた。"
      #  COMの勝ち
      else
        puts "-ウィナー、和也 ウィナー、和也\n"
        puts "和也「カカカっ・・・悪いな・・・！」"
        puts "「あ・・・あぁ・・・！」▼\n"
        gets
        shared = -20000000
        puts "#{shared}ペリカ失った。"   
      end
      @dbh.execute("update gomimera set money = money + #{shared} where id = #{@id}")
      user = @dbh.execute("select money from gomimera where id = #{@id}").fetch[0]
      @money = user[0]
      puts "所持金が#{@money}ペリカになった。▼"
      gets

    end
  end

  # COMが勝負札を選ぶ
  def com_choose
    puts "-プリーズ、セレクト、和也"
    sleep(1)
    puts "\n和也「・・・」▼"
    gets
    # プレイヤーの手札がDOWN・DOWN
    if @player_hand.all? {|card|card.between?(2,7)} 
      # COMの手札がDOWN・DOWN
      if @enemy_hol.all? {|card|card == "down"}
        # 高い方を選択
        @com_select_card = larger(@enemy_hand[0], @enemy_hand[1])
      # COMの手札がDOWN・UP
      elsif @player_hand.one? {|card|card == "down"}
        # 60%の確率でDOWNの値を選択
        if choose(60)
          @com_select_card = @enemy_hand[@enemy_hol.index("down")]
        else
          @com_select_card = @enemy_hand[@enemy_hol.index("up")]
        end        
      # COMの手札がUP・UP
      else
        # 低い方を選択
        @com_select_card = smaller(@enemy_hand[0], @enemy_hand[1]) 
      end
    # プレイヤーの手札がDOWN・UP
    elsif @player_hand.one? {|card|card.between?(2,7)}
      if @enemy_hol.all? {|card|card == "down"}
        # 低い方を選択
        @com_select_card = smaller(@enemy_hand[0], @enemy_hand[1])
      elsif @player_hand.one? {|card|card == "down"}
        # 60%の確率でDOWNの値を選択
        if choose(60)
          @com_select_card = @enemy_hand[@enemy_hol.index("down")]
        else
          @com_select_card = @enemy_hand[@enemy_hol.index("up")]
        end
      else
        # 65%の確率で低い方を選択
        if choose(65)
          @com_select_card = smaller(@enemy_hand[0], @enemy_hand[1])
        else
          @com_select_card = larger(@enemy_hand[0], @enemy_hand[1])
        end
      end
    # プレイヤーの手札がUP・UP
    else
      if @enemy_hol.all? {|card|card == "down"}
        # 低い方を選択
        @com_select_card = smaller(@enemy_hand[0], @enemy_hand[1])
      elsif @player_hand.one? {|card|card == "down"}
        # 70%の確率でDOWNの値を選択
        if choose(70)
          @com_select_card = @enemy_hand[@enemy_hol.index("down")]
        else
          @com_select_card = @enemy_hand[@enemy_hol.index("up")]
        end
      else
        # 80%の確率で高い方を選択
        if choose(80)
          @com_select_card = larger(@enemy_hand[0], @enemy_hand[1])
        else
          @com_select_card = smaller(@enemy_hand[0], @enemy_hand[1])
        end
      end
    end
    # COMの手札の中からCOMが選択した勝負札を削除する
    @enemy_hand.delete_at(@enemy_hand.index(@com_select_card))
  end
  
  def rule
    puts "\e[H\e[2J"
    puts "#{@name}とオレ！どっちが強いか？正しいか？裁いてもらおう！「マザー・ソフィ」に！▼"
    gets
    puts "ワン・ポーカー、このポーカーマシン「マザー・ソフィ」に愛されなきゃどうにもならない！
まずこれが、ワン・ポーカーのカードの強さ順だ」▼"
    gets
    puts "
     DOWN  　| 　　 UP
2<3<4<5<6<7　<　8<9<10<11<12<13<1"
    puts "▼\n"
    gets
    puts "和也「ストレート、フラッシュ・・・といった役の強弱はなし・・・
なんせ・・・1枚でやるポーカーだ、強さは数の強弱のみ。マークの強弱もなし▼"
    gets
    puts "つまり・・・1(エース)が一番強い！
が・・・1つだけ特例！1(エース)だが・・・唯一「2」だけには負ける。
Eカードと同じ仕組みさ。【皇帝】が【奴隷】に刺されるように・・・だ！▼"
    gets
    puts "マザー・ソフィから最初に・・・二人に2枚のカードが配られる。
互いに2枚ずつ持って、そのうち1枚を選んで勝負って訳だ・・・！▼"
    gets
    puts "これが実に悩ましい！身を焼く苦行さ！
2枚のカードのおおよそが、ともにわかる形の勝負だから・・・！▼"
    gets
    puts "マザー・ソフィが伝えてくれる・・・配られた2枚ずつのカード、それがUPかDOWNかを・・・！
    UPってのは8から上、DOWNってのは7から下だ！
お互いのこのUP・DOWNの状況を・・・互いに知りながらの勝負だ！面白そうだろ・・・？▼"
    gets
    puts "選択したカードは破棄され、また新たな1枚がマザー・ソフィから補充される。
後はこの繰り返し・・・▼"
    gets
    puts "そして一番重要な賭け金・・・勝負は1BET・・・2000万ペリカ・・・！
ククク・・・普通の感覚では身を切られるような大金だ・・・！
全5ラウンド消化するが・・・もちろんその前に#{@name}が破滅すれば終了さ・・・。
説明は以上だ・・・ククク・・・！」▼"
    gets 
  end
end
