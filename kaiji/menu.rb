require 'rdbi'
require 'rdbi-driver-sqlite3'
require './underground.rb'
require './finance.rb'
require './e_card.rb'
require './dice_battle.rb'
require './oneporker.rb'
module  Menu
  include Underground	
  include Finance
  include ECard
  include DiceBattle
  include OnePorker
  def main_menu
    while true
      money_and_debt = @dbh.execute("select money, debt from gomimera where id = #{@id}").fetch[0]
      @money =  money_and_debt[0]
      @debt = money_and_debt[1]
      if 0 >= @money && @debt >= 200000000
        goto_underground
        break;
      end
      puts "\e[H\e[2J"
      puts "「さて、どこに行くか・・・」"
      puts "-------------------"
      puts "1.Eカード"
      puts "2.チンチロリン"
      puts "3.ワン・ポーカー"
      puts "4.スロットマシン"
      puts "5.ブラックジャック"
      puts "6.借金"
      puts "7.ランキング"
      puts "8.地下行リスト"
      puts "9.戻る"
      cmd = gets.chomp.to_i
      case 
        when cmd == 1
      	  ecard
        when cmd == 2
      	  dice_battle
        when cmd == 3
      	  oneporker
        when cmd == 4
          puts "黒服「ここは工事中だ・・・今後のアップデートを待つんだな・・・」▼"
          gets
        when cmd == 5
          puts "黒服「ここは工事中だ・・・今後のアップデートを待つんだな・・・」▼"
          gets
        when cmd == 6
      	  acceptance
        when cmd == 7
      	  puts "「債務者ランキング・・・？」\n"
          sleep(1)
      	  rank = @dbh.execute("select name,debt from gomimera where ug_flg = 0 order by debt desc limit 5")
      	  num = 1
      	  rank.each do |row|
            puts "-----------------------"
            puts "第#{num}位"
            puts row
            num += 1
          end
          rank.finish
          puts "\n(チッ・・・悪趣味なやつらだ・・・)▼"
          gets
      when cmd == 8
        puts "「地下行リスト・・・借金を返せないやつは地下労働施設送りって訳か…」\n"
        sleep(1)
        prisoner = @dbh.execute("select name, imprisonment_at from gomimera inner join underground on gomimera.id = underground.gomimera_id;")
        prisoner.each do |row|
          puts "-----------------------"
          puts row
        end
        prisoner.finish
        puts "\n(地下になんか行かねぇ・・・勝つっ・・・！)▼"
        gets
      when cmd == 9
      	break;
      end
    end
  end

end

#menu = Menu.new
#menu.main_menu
