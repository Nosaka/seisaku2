# coding: utf-8

class Juggler
  def initialize
    @arr = ["ピエロ","ブドウ","ベル","リプレイ","バー","チェリー","7"]
    @settei = [1,1,1,1,2,2,2,3,3,3,4,4,5,5,6]
  end

  def run
    #遊戯開始前に設定を1~6のランダムで設定
  	settei = @settei.sample(1)
  	case settei
  	  when 1
  	    @probability = 168
  	  when 2
  	  	@probability = 159
  	  when 3
  	  	@probability = 151
  	  when 4
  	  	@probability = 143
  	  when 5
        @probability = 134
  	  when 6
  	  	@probability = 128
    end

    #遊戯開始
    while true
      puts "1: bet"
      puts "9: quit"
      cmd = gets.chomp.to_i
      case
        when cmd == 1
          puts "Enter: start"
          stop
        when cmd == 9
          puts "see you !"
          break;
      end
    end
  end 

  #リールストップ
  def stop
  	puts "\e[H\e[2J"
    puts "Enter: stop"				
    reel = []
    3.times do
      gets
      puts "\e[H\e[2J"
      reel.push(@arr.sample(1))
      puts reel.join("|")
    end
  	#単独ボーナスの抽選
  	win = rand 2#@probability
  	if win == 1
  	  gogo_chance
  	end

  end

  def gogo_chance
    puts "  ∧ ∧ ∧ "
    puts "＜  GOGO！＞ "
    puts "  ∨ ∨ ∨ "
    puts "   CHANCE! "
    sleep 2
    #BIGボーナスかREGボーナスかおよそ10：15の割合で抽選
    big_or_reg = rand 25
    if big_or_reg > 10
       big_bonus
    else
       reguler_bonus
    end
  end

  def big_bonus
  	puts "BIG BONUS!!"
	puts "______   ______   ______  "
	puts "|____  | |____  | |____  | "
	puts "    / /      / /      / /  "
	puts "   / /      / /      / /  " 
	puts "  / /      / /      / /   "
	puts " /_/      /_/      /_/    "                         
  end

  def reguler_bonus
  	puts "REGULER BONUS!"
    puts " ______   ______   ____ "            
    puts "|____  | |____  | |  _ l "           
    puts "    / /      / /  | |_) | __ _ _ __ "
    puts "   / /      / /   |  _ < / _` | '__|"
    puts "  / /      / /    | |_) | (_| | |   "
    puts " /_/      /_/     |____/ l__,_|_|   "                          
  end

end
juggler = Juggler.new
juggler.run